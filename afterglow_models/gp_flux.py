from afterglow_models.sf_flux_legacy import simulation_templates
from afterglow_data.misc import convert_hz_to_a
from afterglow_data.misc import in_range
from afterglow_data.extinction import application
from gaussian_processes.gp import GP
import gpflow
import numpy as np
from gpflow.utilities import print_summary, set_trainable
import tensorflow as tf


TINY_FLUX = 1e-30
TINY_LIKELIHOOD = -1e300
HUGE_CHI2 = 1e300


class HeteroskedasticGaussian(gpflow.likelihoods.Likelihood):
    def __init__(self, **kwargs):
        # this likelihood expects a single latent function F, and two columns in the data matrix Y:
        # print('init')
        self.NoiseVar = None
        self.variance = 0
        super().__init__(latent_dim=1, observation_dim=2, **kwargs)
        # print('init2')

    def _log_prob(self, F, Y):
        # print('_log_prob')
        # log_prob is used by the quadrature fallback of variational_expectations and predict_log_density.
        # Because variational_expectations is implemented analytically below, this is not actually needed,
        # but is included for pedagogical purposes.
        # Note that currently relying on the quadrature would fail due to https://github.com/GPflow/GPflow/issues/966
        return gpflow.logdensities.gaussian(Y, F, self.NoiseVar)

    def _variational_expectations(self, Fmu, Fvar, Y):
        # print('_variational_expectations')
        return (
            -0.5 * np.log(2 * np.pi)
            - 0.5 * tf.math.log(self.NoiseVar)
            - 0.5 * (tf.math.square(Y - Fmu) + Fvar) / self.NoiseVar
        )

    # The following two methods are abstract in the base class.
    # They need to be implemented even if not used.

    def _predict_log_density(self, Fmu, Fvar, Y):
        raise NotImplementedError

    def _predict_mean_and_var(self, Fmu, Fvar):
        raise NotImplementedError


class Scalefit:
    def __init__(self, wind=False,
                 f_nu_obs=None,
                 t_obs=None,
                 nu_obs=None,
                 err_obs=None,
                 log_flux=True,
                 log_time=True,
                 log_nu=True,
                 use_errorbars=False,
                 fractional_flux=False,
                 kernel_type='RBF'):
        self.table_filename = '/Users/mda/bin/scalefit-tables/tableISM.h5'
        if wind:
            self.table_filename = '/Users/mda/bin/scalefit-tables/tableWIND.h5'
        self.sf = simulation_templates(table=self.table_filename)
        self.log_flux = log_flux
        self.log_time = log_time
        self.log_nu = log_nu
        self.use_errorbars = use_errorbars
        self.fractional_flux = fractional_flux
        self.kernel_type = kernel_type
        if self.log_flux:
            self.f_nu_obs = f_nu_obs[f_nu_obs - err_obs > 0]
            self.t_obs = t_obs[f_nu_obs - err_obs > 0]
            self.nu_obs = nu_obs[f_nu_obs - err_obs > 0]
            self.err_obs = err_obs[f_nu_obs - err_obs > 0]
        else:
            self.f_nu_obs = f_nu_obs
            self.t_obs = t_obs
            self.nu_obs = nu_obs
            self.err_obs = err_obs

        self.log10_f_nu_obs = np.log10(self.f_nu_obs)
        self.log10_t_obs = np.log10(self.t_obs)
        self.log10_nu_obs = np.log10(self.nu_obs)
        self.log10_err_obs = np.log10(self.f_nu_obs + self.err_obs) - self.log10_f_nu_obs

        x1 = self.log10_t_obs if self.log_time else self.t_obs
        x2 = self.log10_nu_obs if self.log_nu else self.nu_obs
        self.X = np.column_stack((x1, x2))
        self.Y = None
        self.gp = None
        if self.log_flux:
            if self.use_errorbars:
                self.variance = self.log10_err_obs ** 2
            else:
                self.variance = 1
        else:
            if self.use_errorbars:
                self.variance = self.err_obs ** 2
            else:
                self.variance = 1
        pass

    def lnlikelihood(self, sf_params, log10_hyperparameters,
                     host_type=None,
                     a_v_host=None,
                     persistent_bands=None,
                     persistent_flux=None):
        a, l1, l2, white_noise, p1, p2 = 10 ** log10_hyperparameters
        # k1 = gpflow.kernels.RationalQuadratic(variance=a, lengthscales=l1, alpha=p1, active_dims=[0])
        # k2 = gpflow.kernels.RationalQuadratic(variance=1, lengthscales=l2, alpha=p2, active_dims=[1])
        k1 = gpflow.kernels.SquaredExponential(variance=a, lengthscales=[l1, l2], active_dims=[0, 1])
        # k2 = gpflow.kernels.Cosine(variance=1, lengthscales=l1, active_dims=[0])
        k = k1

        f_nu_model = self.f_nu(sf_params=sf_params, t_obs=self.t_obs, nu_obs=self.nu_obs, host_type=host_type,
                               a_v_host=a_v_host, persistent_bands=persistent_bands,
                               persistent_flux=persistent_flux)
        f_nu_model[f_nu_model <= 0] = TINY_FLUX
        log10_f_nu_model = np.log10(f_nu_model)
        if self.log_flux:
            if self.fractional_flux:
                self.Y = (self.log10_f_nu_obs - log10_f_nu_model) / self.log10_err_obs
            else:
                self.Y = self.log10_f_nu_obs - log10_f_nu_model
        else:
            if self.fractional_flux:
                # self.Y = (self.f_nu_obs - f_nu_model) / self.err_obs
                self.Y = np.arcsinh((self.f_nu_obs - f_nu_model) / min(self.err_obs))
            else:
                self.Y = self.f_nu_obs - f_nu_model

        if self.use_errorbars:
            # y_with_err = np.hstack([self.Y, self.variance * white_noise])
            # y_with_err = np.column_stack((self.Y / min(np.sqrt(self.variance)),
            #                               self.variance * white_noise / min(self.variance)))
            # likelihood = HeteroskedasticGaussian()
            scaling_factor = 1 #np.sqrt(min(self.variance))
            Y = self.Y.reshape(-1, 1) / scaling_factor
            var = self.variance[:, None] / (scaling_factor ** 2)
            # likelihood.NoiseVar = var
            # print(Y, var)
            # gpflow.likelihoods.Gaussian.DEFAULT_VARIANCE_LOWER_BOUND = -1
            likelihood = gpflow.likelihoods.Gaussian(variance=min(var), variance_lower_bound=-1)
            # print('before model')
            self.gp = gpflow.models.GPR(data=(self.X, Y), kernel=k)
            self.gp.likelihood = likelihood
            # print('after model')
            # gpflow.models.GPModel.__init__(self.gp, gpflow.DataHolder(self.X), gpflow.DataHolder(Y),
            #                                k, likelihood, None)
            # print('before hack')
            # print(self.X.shape)
            # print(y_with_err.shape)
        else:
            self.gp = gpflow.models.GPR(data=(self.X, self.Y.reshape(-1, 1)), kernel=k,
                                        mean_function=None, noise_variance=white_noise)

        try:
            if self.use_errorbars:
                # print('1')
                res = float(self.gp.log_marginal_likelihood())
                # print('2')
                # print(res)
            else:
                res = self.gp.log_marginal_likelihood()
            if np.isnan(res) or np.isinf(res):
                return TINY_LIKELIHOOD
            return res
        except:
            return TINY_LIKELIHOOD

    # def lnlikelihood(self, sf_params, log10_hyperparameters,
    #                  host_type=None,
    #                  a_v_host=None,
    #                  persistent_bands=None,
    #                  persistent_flux=None):
    #
    #     f_nu_model = self.f_nu(sf_params=sf_params, t_obs=self.t_obs, nu_obs=self.nu_obs, host_type=host_type,
    #                            a_v_host=a_v_host, persistent_bands=persistent_bands,
    #                            persistent_flux=persistent_flux)
    #     f_nu_model[f_nu_model <= 0] = TINY_FLUX
    #     log10_f_nu_model = np.log10(f_nu_model)
    #     if self.log_flux:
    #         self.Y = (self.log10_f_nu_obs - log10_f_nu_model) / self.log10_err_obs
    #     else:
    #         self.Y = ((self.f_nu_obs - f_nu_model) / self.err_obs)
    #
    #     if np.any(np.isnan(self.Y)) or np.any(np.isinf(self.Y)):
    #         return TINY_LIKELIHOOD
    #     a = 1
    #     l1 = 1
    #     l2 = 1
    #     white_noise = 1e-5
    #     # k1 = gpflow.kernels.RationalQuadratic(variance=a, lengthscales=l1, alpha=p1, active_dims=[0])
    #     # k2 = gpflow.kernels.RationalQuadratic(variance=1, lengthscales=l2, alpha=p2, active_dims=[1])
    #
    #     k1 = gpflow.kernels.Matern52(variance=a, lengthscales=[l1, l2])
    #     k = k1
    #
    #     # try:
    #     self.gp = gpflow.models.GPR(data=(self.X, self.Y.reshape(-1, 1)), kernel=k, noise_variance=white_noise)
    #     set_trainable(self.gp.likelihood.variance, False)
    #     # print_summary(self.gp)
    #     opt = gpflow.optimizers.Scipy()
    #
    #     try:
    #         opt.minimize(self.gp.training_loss, self.gp.trainable_variables, options=dict(maxiter=10))
    #     # print_summary(self.gp)
    #     except:
    #         return TINY_LIKELIHOOD
    #
    #     res = self.gp.log_marginal_likelihood()
    #     if np.isnan(res) or np.isinf(res):
    #         return TINY_LIKELIHOOD
    #     return res
    #     # except:
    #     #     return TINY_LIKELIHOOD

    def predict(self, t_model, nu_model):
        log10_t_model = np.log10(t_model)
        log10_nu_model = np.log10(nu_model)
        x1 = log10_t_model if self.log_time else t_model
        x2 = log10_nu_model if self.log_nu else nu_model
        X_model = np.column_stack((x1, x2))
        mean, std = self.gp.predict_y(X_model)
        mean = mean[:, 0]
        std = 1.96 * np.sqrt(std[:, 0])
        return mean, mean - std, mean + std

    def f_nu_gp(self, sf_params, t_model, nu_model,
                host_type=None,
                a_v_host=None,
                persistent_bands=None,
                persistent_flux=None, ):
        f_nu_model = self.f_nu(sf_params=sf_params, t_obs=t_model, nu_obs=nu_model, host_type=host_type,
                               a_v_host=a_v_host, persistent_bands=persistent_bands, persistent_flux=persistent_flux)
        f_nu_model[f_nu_model <= 0] = TINY_FLUX
        log10_f_nu_model = np.log10(f_nu_model)
        log10_t_model = np.log10(t_model)
        log10_nu_model = np.log10(nu_model)
        x1 = log10_t_model if self.log_time else t_model
        x2 = log10_nu_model if self.log_nu else nu_model
        X_model = np.column_stack((x1, x2))
        mean, cov = self.gp.predict(X_model, return_cov=True)
        std = np.sqrt(np.diag(cov)) if self.use_errorbars else np.sqrt(np.diag(cov))
        if self.log_flux:
            mean = mean * self.log10_err_obs
            std = std * self.log10_err_obs
        else:
            mean = mean * self.err_obs
            std = std * self.err_obs
        mean_res = 10 ** (log10_f_nu_model + mean) if self.log_flux else f_nu_model + mean
        lower_res = 10 ** (log10_f_nu_model + mean - std) if self.log_flux else f_nu_model + mean - std
        upper_res = 10 ** (log10_f_nu_model + mean + std) if self.log_flux else f_nu_model + mean + std
        return mean_res, lower_res, upper_res

    def f_nu(self, sf_params, t_obs, nu_obs,
             host_type=None,
             a_v_host=None,
             persistent_bands=None,
             persistent_flux=None):
        f_nu_arr = np.array(self.sf.Fnu(params=sf_params, times=t_obs, nu=nu_obs))
        # Apply extinction law
        if host_type is not None and a_v_host is not None:
            z = sf_params[self.sf.index_z_]
            lambda_rest = convert_hz_to_a(nu_obs) * (1 + z)
            f_nu_arr = application(lam=lambda_rest, flux=f_nu_arr, Av=a_v_host, law=host_type)
        # Add persistent flux
        if persistent_bands is not None and persistent_flux is not None:
            for b, band in enumerate(persistent_bands):
                b_mask = in_range(nu_obs, band)
                f_nu_arr[b_mask] = f_nu_arr[b_mask] + persistent_flux[b]
        return f_nu_arr
