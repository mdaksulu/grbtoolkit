from afterglow_models.sf_flux_legacy import simulation_templates
from afterglow_models.sf_bpl import SF_BPL
import numpy as np


class Scalefit:
    def __init__(self, wind=False):
        self.table_filename = '/Users/mda/bin/scalefit-tables/tableISM.h5'
        if wind:
            self.table_filename = '/Users/mda/bin/scalefit-tables/tableWIND.h5'
        self.sf = simulation_templates(table=self.table_filename)
        pass

    def f_nu(self, params, t_obs, nu_obs, legacy=True, weighted=True):
        if legacy:
            return self.sf.Fnu(params=params, times=t_obs, nu=nu_obs)
        else:
            if not weighted:
                res = np.zeros(len(t_obs))
                peak_flux, nu_m, nu_c, nu_a = self.sf.flux_parameters(times=t_obs, params=params)
                p = params[self.sf.index_p_]
                nu_a2 = nu_a ** (10. / (3. * (p + 4.))) * nu_m ** ((2. + 3. * p) / (3. * (p + 4.)))
                nu_a3 = (nu_a ** (10. / (3 * (p + 5.))) *
                         nu_m ** ((2. + 3. * p) / (3. * (p + 5.))) *
                         nu_c ** (1. / (p + 5.)))
                nu_a3alt = (nu_a ** (10. / (3. * (p + 5.))) * nu_m ** ((3. * p + 5.) / (3. * (p + 5.))))
                nu_a4 = (nu_a ** (2. / 3.) * nu_m ** (1. / 3.))
                nu_a5 = (nu_a * nu_m ** 0.5 * nu_c ** (-0.5))
                for i, _peak_flux in enumerate(peak_flux):
                    sbpl = SF_BPL(f_peak=_peak_flux, nu_a=nu_a[i], nu_a2=nu_a2[i], nu_a3=nu_a3[i],
                                  nu_a3alt=nu_a3alt[i], nu_a4=nu_a4[i], nu_a5=nu_a5[i], nu_m=nu_m[i], nu_c=nu_c[i], p=p)
                    res[i] = sbpl.evaluate([nu_obs[i]])
            else:
                res = np.zeros(len(t_obs))
                peak_flux, nu_m, nu_c, nu_a = self.sf.flux_parameters(times=t_obs, params=params)
                p = params[self.sf.index_p_]
                nu_a2 = nu_a ** (10. / (3. * (p + 4.))) * nu_m ** ((2. + 3. * p) / (3. * (p + 4.)))
                nu_a3 = (nu_a ** (10. / (3 * (p + 5.))) *
                         nu_m ** ((2. + 3. * p) / (3. * (p + 5.))) *
                         nu_c ** (1. / (p + 5.)))
                nu_a3alt = (nu_a ** (10. / (3. * (p + 5.))) * nu_m ** ((3. * p + 5.) / (3. * (p + 5.))))
                nu_a4 = (nu_a ** (2. / 3.) * nu_m ** (1. / 3.))
                nu_a5 = (nu_a * nu_m ** 0.5 * nu_c ** (-0.5))
                for i, _peak_flux in enumerate(peak_flux):
                    # w1 = (nu_c[i] / nu_a[i]) ** 2.5 * 1
                    # w2 = (nu_c[i] / nu_m[i]) ** 2.5 * 1
                    # w3 = (nu_a3[i] / nu_m[i]) ** 2.5 * 1
                    # w3alt = (nu_a2[i] / nu_c[i]) ** 2.5 * 1
                    # w4 = (nu_m[i] / nu_c[i]) ** 2.5 * 1
                    # w5 = (nu_m[i] / nu_a[i]) ** 2.5 * 1
                    w1 = (nu_c[i] / nu_a[i]) ** 2.5 * 1
                    w2 = (nu_c[i] / nu_m[i]) ** 2.5 * 1
                    w3 = (nu_a3[i] / nu_m[i]) ** 2.5 * 1
                    w3alt = (nu_a3alt[i] / nu_c[i]) ** 2.5 * 1
                    w4 = (nu_m[i] / nu_c[i]) ** 2.5 * 1
                    w5 = (nu_m[i] / nu_a5[i]) ** 2.5 * 1
                    res_temp = np.zeros(6)
                    for j in range(6):
                        sbpl = SF_BPL(f_peak=_peak_flux, nu_a=nu_a[i], nu_a2=nu_a2[i], nu_a3=nu_a3[i],
                                      nu_a3alt=nu_a3alt[i], nu_a4=nu_a4[i], nu_a5=nu_a5[i], nu_m=nu_m[i], nu_c=nu_c[i],
                                      p=p,
                                      spec_type=j)
                        res_temp[j] = sbpl.evaluate([nu_obs[i]])
                    res[i] = ((w1 * res_temp[0] + w2 * res_temp[1] + w3 * res_temp[2] +
                               w3alt * res_temp[3] + w4 * res_temp[4] + w5 * res_temp[5]) /
                              (w1 + w2 + w3 + w3alt + w4 + w5))
            return res

    def spec_type(self, params, t_obs):
        res = np.zeros(len(t_obs))
        nu_b = []
        peak_flux, nu_m, nu_c, nu_a = self.sf.flux_parameters(times=t_obs, params=params)
        p = params[self.sf.index_p_]
        nu_a2 = nu_a ** (10. / (3. * (p + 4.))) * nu_m ** ((2. + 3. * p) / (3. * (p + 4.)))
        nu_a3 = (nu_a ** (10. / (3 * (p + 5.))) *
                 nu_m ** ((2. + 3. * p) / (3. * (p + 5.))) *
                 nu_c ** (1. / (p + 5.)))
        nu_a3alt = (nu_a ** (10. / (3. * (p + 5.))) * nu_m ** ((3. * p + 5.) / (3. * (p + 5.))))
        nu_a4 = (nu_a ** (2. / 3.) * nu_m ** (1. / 3.))
        nu_a5 = (nu_a * nu_m ** 0.5 * nu_c ** (-0.5))
        for i, _peak_flux in enumerate(peak_flux):
            sbpl = SF_BPL(f_peak=_peak_flux, nu_a=nu_a[i], nu_a2=nu_a2[i], nu_a3=nu_a3[i],
                          nu_a3alt=nu_a3alt[i], nu_a4=nu_a4[i], nu_a5=nu_a5[i], nu_m=nu_m[i], nu_c=nu_c[i], p=p)
            res[i] = sbpl.spec_type
            nu_b.append(sbpl.nu_b)
        nu_b = np.array(nu_b)
        return res  # , nu_b, peak_flux, nu_m, nu_c, nu_a, nu_a2, nu_a3, nu_a3alt, nu_a4, nu_a5
