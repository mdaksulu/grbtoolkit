import fileinput

import numpy as np

from . import file_reader
from . import misc


def read_setting(setting, boxfit_settings_filename='boxfitsettings.txt'):
    result = None
    f = fileinput.FileInput(boxfit_settings_filename)
    for line in f:
        if line.strip().startswith(setting + ' = '.format(setting)):
            result = line.strip().split('=')[1].strip()
    f.close()
    if result is None:
        result = 0
    return result


def read_data_filename(boxfit_settings_filename='boxfitsettings.txt'):
    data_filename = read_setting('data_filename', boxfit_settings_filename=boxfit_settings_filename).split('"')[1]
    return data_filename


def read_islog(boxfit_settings_filename='boxfitsettings.txt'):
    islog = [0, 1, 1, 0, 0, 1, 1, 1]
    islog[0] = int(read_setting('theta_0_log', boxfit_settings_filename=boxfit_settings_filename))
    host_bands, _ = read_host_bands(boxfit_settings_filename=boxfit_settings_filename)
    log_host = int(read_setting('log_host', boxfit_settings_filename=boxfit_settings_filename))
    for i in range(len(host_bands)):
        islog.append(log_host)
    return islog


def read_parameter_ranges(boxfit_settings_filename='boxfitsettings.txt'):
    parameter_names = ['theta_0', 'E', 'n', 'theta_obs', 'p', 'epsilon_B', 'epsilon_E', 'ksi_N']
    min_params = []
    max_params = []
    for i, name in enumerate(parameter_names):
        min_params.append(float(read_setting(name + '_min', boxfit_settings_filename=boxfit_settings_filename)))
        max_params.append(float(read_setting(name + '_max', boxfit_settings_filename=boxfit_settings_filename)))

    data_filename = read_data_filename(boxfit_settings_filename=boxfit_settings_filename)
    band_margin = float(read_setting('band_margin', boxfit_settings_filename=boxfit_settings_filename))
    df_data, bands = file_reader.read_obs_data(data_filename, accuracy=band_margin)

    fit_host = int(read_setting('fit_host', boxfit_settings_filename=boxfit_settings_filename))
    log_host = int(read_setting('log_host', boxfit_settings_filename=boxfit_settings_filename))
    log_host_minmax_ratio = float(
        read_setting('log_host_minmax_ratio', boxfit_settings_filename=boxfit_settings_filename))
    host_bands = []
    if fit_host == 1:
        no_host_band = int(read_setting('no_host_bands', boxfit_settings_filename=boxfit_settings_filename))
        for i in range(no_host_band):
            host_bands.append(
                float(read_setting('host_band_{0}'.format(i), boxfit_settings_filename=boxfit_settings_filename)))
            parameter_names.append('f_{0}'.format(i))
            mask = misc.get_mask(df_data, host_bands[i], accuracy=band_margin)
            if log_host == 0:
                min_params.append(0)
                max_params.append(min(df_data['Flux'][mask] + df_data['Error'][mask]))
            else:
                max_params.append(min(df_data['Flux'][mask] + df_data['Error'][mask]))
                min_params.append(
                    10 ** (np.log10(min(df_data['Flux'][mask] + df_data['Error'][mask])) + log_host_minmax_ratio))
    return parameter_names, min_params, max_params


def read_host_bands(boxfit_settings_filename='boxfitsettings.txt'):
    host_bands = []
    fit_host = int(read_setting('fit_host', boxfit_settings_filename=boxfit_settings_filename))
    band_margin = 0.03
    if fit_host == 1:
        band_margin = float(read_setting('band_margin', boxfit_settings_filename=boxfit_settings_filename))
        no_host_band = int(read_setting('no_host_bands', boxfit_settings_filename=boxfit_settings_filename))
        for i in range(no_host_band):
            host_bands.append(
                float(read_setting('host_band_{0}'.format(i), boxfit_settings_filename=boxfit_settings_filename)))
    return host_bands, band_margin


def convert_mn_to_physical(mn_params, boxfit_settings_filename='boxfitsettings.txt', min_params=None, max_params=None,
                           islog=None, theta_obs_frac=None):
    if min_params is None or max_params is None:
        _, min_params, max_params = read_parameter_ranges(
            boxfit_settings_filename=boxfit_settings_filename)
    if islog is None:
        islog = read_islog(boxfit_settings_filename=boxfit_settings_filename)
    if theta_obs_frac is None:
        theta_obs_frac = int(
            read_setting('theta_obs_frac', boxfit_settings_filename=boxfit_settings_filename))
    parameters = np.array(min_params)
    free_param_index = []
    for i in range(len(min_params)):
        if min_params[i] != max_params[i]:
            free_param_index.append(i)
    for i, i_real in enumerate(free_param_index):
        if islog[i_real] == 0:
            parameters[i_real] = min_params[i_real] + (max_params[i_real] - min_params[i_real]) * mn_params[i]
        elif islog[i_real] == 1:
            parameters[i_real] = 10 ** (np.log10(min_params[i_real]) + (
                    np.log10(max_params[i_real]) - np.log10(min_params[i_real])) * mn_params[i])
    if theta_obs_frac == 1:
        parameters[3] = parameters[0] * parameters[3]
    return parameters
