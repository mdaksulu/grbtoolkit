from setuptools import setup

setup(
    name='GRBToolkit',
    version='0.0.1',
    packages=['afterglow_models', 'afterglow_data', 'gaussian_processes'],
    url='',
    license='',
    author='Mehmet Deniz Aksulu',
    author_email='aksulumdeniz@gmail.com',
    description='', install_requires=['numpy', 'george', 'scipy', 'gpflow']
)
