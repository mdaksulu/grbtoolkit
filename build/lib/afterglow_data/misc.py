import numpy as np
import astropy.constants as const


def group(unique, accuracy=0.03):
    sorted_unique = sorted(unique)
    result = []
    for i in range(len(sorted_unique)):
        exists = False
        for j in range(len(result)):
            if accuracy < 1.0:
                if sorted_unique[i] * (1 - accuracy) <= result[j] <= sorted_unique[i] * (1 + accuracy):
                    exists = True
            else:
                if sorted_unique[i] / accuracy <= result[j] <= sorted_unique[i] * accuracy:
                    exists = True
        if not exists:
            result.append(sorted_unique[i])
    return np.array(result)


def get_mask(df, val, accuracy=0.03, key='Freq', arr=False):
    if accuracy < 1.0:
        if not arr:
            return (df[key] >= val * (1 - accuracy)) & (df[key] <= val * (1 + accuracy))
        else:
            return (df >= val * (1 - accuracy)) & (df <= val * (1 + accuracy))
    else:
        return get_mask_wide(df, val, accuracy=accuracy, key=key, arr=arr)


def get_mask_wide(df, val, accuracy=0.03, key='Freq', arr=False):
    if not arr:
        return (df[key] >= val / accuracy) & (df[key] <= val * accuracy)
    else:
        return (df >= val / accuracy) & (df <= val * accuracy)


def get_upperlimit_mask(df, arr=False, f_nu_obs=None, err_obs=None):
    if not arr:
        return (df['Flux'] - df['Error'] <= 0) | (df['Flux'] + df['Error'] <= 0) | (df['Error'] == 0)
    else:
        return (f_nu_obs - err_obs <= 0) | (f_nu_obs + err_obs <= 0) | (err_obs == 0)


def get_detection_mask(df, arr=False, f_nu_obs=None, err_obs=None):
    if not arr:
        return (df['Flux'] - df['Error'] > 0) & (df['Flux'] + df['Error'] > 0) & (df['Error'] != 0)
    else:
        return (f_nu_obs - err_obs > 0) & (f_nu_obs + err_obs > 0) & (err_obs != 0)


def in_range(test_val, val, accuracy=0.03):
    return (test_val >= val * (1 - accuracy)) & (test_val <= val * (1 + accuracy))


def convert_hz_to_a(hz):
    return const.c.cgs.value / hz * 1e8