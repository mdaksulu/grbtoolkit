import numpy as np


class XRTTools:
    keV = 2.41799050402417E+17

    def __init__(self):
        pass

    @staticmethod
    def flux_density(flux, gamma, unabs_abs_ratio=None, kev=None, kev_min=0.3, kev_max=10):
        nu_low = kev_min * XRTTools.keV
        nu_high = kev_max * XRTTools.keV
        if kev is None:
            nu_avg = np.sqrt(nu_low * nu_high)
        else:
            nu_avg = kev * XRTTools.keV

        if unabs_abs_ratio is not None:
            unabs_flux = flux * unabs_abs_ratio
        else:
            unabs_flux = flux

        f_nu = np.zeros(len(flux))
        beta = gamma - 1
        if hasattr(beta, "__len__"):
            mask_unity = beta == 1
            mask_small = beta < 1
            mask_large = beta > 1
        else:
            mask_unity = np.full(len(unabs_flux), beta == 1)
            mask_small = np.full(len(unabs_flux), beta < 1)
            mask_large = np.full(len(unabs_flux), beta > 1)
        f_nu[mask_unity] = unabs_flux[mask_unity] / nu_avg * np.log(nu_high / nu_low)**-1
        f_nu[mask_small] = ((1 - beta[mask_small]) * unabs_flux[mask_small] / nu_high *
                            (nu_high / nu_low) ** (beta[mask_small] / 2.) *
                            (1 - (nu_low / nu_high) ** (1 - beta[mask_small])) ** -1)
        f_nu[mask_large] = ((beta[mask_large] - 1) * unabs_flux[mask_large] / nu_low *
                            (nu_low / nu_high) ** (beta[mask_large] / 2.) *
                            (1 - (nu_high / nu_low) ** (1 - beta[mask_large])) ** -1)
        f_nu = f_nu * 1e26  # Convert to mJy
        return f_nu, nu_avg
