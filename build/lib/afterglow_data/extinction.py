import numpy as np


def pow_temp(a, b):
    return a ** b


def correction(lam, flux, Av, law):
    """
    Correct flux densities for extinction.

    Input:
            LAM: wavelength in \AA
            FLUX: flux densities (whatever unit)
            AV: extinction in the V band
            LAW: assumed extinction curve:
                0 - MW curve from Pei 1992
                1 - LMC curve from Pei 1992
                2 - SMC curve from Pei 1992
                3 - MW curve from Cardelli+89
                4 - Starburst galaxy curve from Calzetti+2000

        Output:
            Input flux corrected for extinction.
        lamC???
    """

    lam = lam / 10000

    if law is 0:
        extinct = 165. / (pow_temp(lam / 0.047, 2.) + pow_temp(0.047 / lam, 2.) + 90.) + 14. / (
                pow_temp(lam / 0.08, 6.5) + pow_temp(0.08 / lam, 6.5) + 4.) + 0.045 / (
                          pow_temp(lam / 0.22, 2.) + pow_temp(0.22 / lam, 2.) - 1.95) + 0.002 / (
                          pow_temp(lam / 9.7, 2.) + pow_temp(9.7 / lam, 2.) - 1.95) + 0.002 / (
                          pow_temp(lam / 18., 2.) + pow_temp(18. / lam, 2.) - 1.80) + 0.012 / (
                          pow_temp(lam / 25., 2.) + pow_temp(25. / lam, 2.) + 0.)
        flux = flux * pow_temp(10, 0.4 * 1.32 * Av * extinct)

    elif law is 1:
        extinct = 175. / (pow_temp(lam / 0.046, 2.) + pow_temp(0.046 / lam, 2.) + 90.) + 19. / (
                pow_temp(lam / 0.08, 4.5) + pow_temp(0.08 / lam, 4.5) + 5.50) + 0.023 / (
                          pow_temp(lam / 0.22, 2.) + pow_temp(0.22 / lam, 2.) - 1.95) + 0.005 / (
                          pow_temp(lam / 9.7, 2.) + pow_temp(9.7 / lam, 2.) - 1.95) + 0.006 / (
                          pow_temp(lam / 18., 2.) + pow_temp(18. / lam, 2.) - 1.80) + 0.020 / (
                          pow_temp(lam / 25., 2.) + pow_temp(25. / lam, 2.) + 0.)
        flux = flux * pow_temp(10, 0.4 * 1.34 * Av * extinct)

    elif law is 2:
        extinct = 185. / (pow_temp(lam / 0.042, 2.) + pow_temp(0.042 / lam, 2.) + 90.) + 27. / (
                pow_temp(lam / 0.08, 4.0) + pow_temp(0.08 / lam, 4.0) + 5.50) + 0.005 / (
                          pow_temp(lam / 0.22, 2.) + pow_temp(0.22 / lam, 2.) - 1.95) + 0.010 / (
                          pow_temp(lam / 9.7, 2.) + pow_temp(9.7 / lam, 2.) - 1.95) + 0.012 / (
                          pow_temp(lam / 18., 2.) + pow_temp(18. / lam, 2.) - 1.80) + 0.030 / (
                          pow_temp(lam / 25., 2.) + pow_temp(25. / lam, 2.) + 0.)
        flux = flux * pow_temp(10, 0.4 * 1.32 * Av * extinct)

    elif law is 3:
        x = 1 / lam
        if hasattr(x, '__len__'):
            a = 0
            b = 0
            for i, _x in enumerate(x):
                if 0.2 < _x <= 1.1:
                    a = 0.574 * pow_temp(_x, 1.61)
                    b = -0.527 * pow_temp(_x, 1.61)
                elif 1.1 < _x <= 3.3:
                    y = _x - 1.81
                    a = 1 + 0.17699 * y - 0.50447 * y * y - 0.02427 * pow_temp(y, 3) + 0.72085 * pow_temp(y,
                                                                                                          4) + 0.01979 * pow_temp(
                        y, 5) - 0.77530 * pow_temp(y, 6) + 0.32999 * pow_temp(y, 7)
                    b = 1.41338 * y + 2.28305 * y * y + 1.07233 * pow_temp(y, 3) - 5.38434 * pow_temp(y,
                                                                                                      4) - 0.62251 * pow_temp(
                        y,
                        5) + \
                        5.30260 * pow_temp(y, 6) - 2.09002 * pow_temp(y, 7)
                elif 3.3 < _x < 8.0:
                    if _x < 5.9:
                        Fa = 0
                        Fb = 0
                    else:
                        Fa = -0.04473 * pow_temp(_x - 5.9, 2) - 0.009779 * pow_temp(_x - 5.9, 3)
                        Fb = 0.2130 * pow_temp(_x - 5.9, 2) + 0.1207 * pow_temp(_x - 5.9, 3)

                    a = 1.752 - 0.316 * _x - 0.104 / (pow_temp(_x - 4.67, 2) + 0.341) + Fa
                    b = -3.090 + 1.825 * _x + 1.206 / (pow_temp(_x - 4.62, 2) + 0.263) + Fb

                Alam = Av * (a + b / 3.1)
                flux[i] = flux[i] * pow_temp(10, 0.4 * Alam)
        else:
            if 0.2 < x <= 1.1:
                a = 0.574 * pow_temp(x, 1.61)
                b = -0.527 * pow_temp(x, 1.61)
            elif 1.1 < x <= 3.3:
                y = x - 1.81
                a = 1 + 0.17699 * y - 0.50447 * y * y - 0.02427 * pow_temp(y, 3) + 0.72085 * pow_temp(y,
                                                                                                      4) + 0.01979 * pow_temp(
                    y, 5) - 0.77530 * pow_temp(y, 6) + 0.32999 * pow_temp(y, 7)
                b = 1.41338 * y + 2.28305 * y * y + 1.07233 * pow_temp(y, 3) - 5.38434 * pow_temp(y,
                                                                                                  4) - 0.62251 * pow_temp(
                    y,
                    5) + \
                    5.30260 * pow_temp(y, 6) - 2.09002 * pow_temp(y, 7)
            elif 3.3 < x < 8.0:
                if x < 5.9:
                    Fa = 0
                    Fb = 0
                else:
                    Fa = -0.04473 * pow_temp(x - 5.9, 2) - 0.009779 * pow_temp(x - 5.9, 3)
                    Fb = 0.2130 * pow_temp(x - 5.9, 2) + 0.1207 * pow_temp(x - 5.9, 3)

                a = 1.752 - 0.316 * x - 0.104 / (pow_temp(x - 4.67, 2) + 0.341) + Fa
                b = -3.090 + 1.825 * x + 1.206 / (pow_temp(x - 4.62, 2) + 0.263) + Fb

            Alam = Av * (a + b / 3.1)
            flux = flux * pow_temp(10, 0.4 * Alam)

    elif law is 4:
        if lam < 0.63:
            extin = 2.659 * (-2.156 + 1.509 / lam - 0.198 / pow_temp(lam, 2.0) + 0.011 / pow_temp(lam, 3.0)) + 4.05
        else:
            extin = 2.659 * (-1.857 + 1.040 / lam) + 4.05
        flux = flux * pow_temp(10, 0.4 * extin * Av / 4.05)

    return flux


def application(lam, flux, Av, law):
    """
    Correct flux densities for extinction.

    Input:
            LAM: wavelength in \AA
            FLUX: flux densities (whatever unit)
            AV: extinction in the V band
            LAW: assumed extinction curve:
                0 - MW curve from Pei 1992
                1 - LMC curve from Pei 1992
                2 - SMC curve from Pei 1992
                3 - MW curve from Cardelli+89
                4 - Starburst galaxy curve from Calzetti+2000

        Output:
            Input flux corrected for extinction.
        lamC???
    """

    lam = lam / 10000

    if law is 0:
        extinct = 165. / (pow_temp(lam / 0.047, 2.) + pow_temp(0.047 / lam, 2.) + 90.) + 14. / (
                pow_temp(lam / 0.08, 6.5) + pow_temp(0.08 / lam, 6.5) + 4.) + 0.045 / (
                          pow_temp(lam / 0.22, 2.) + pow_temp(0.22 / lam, 2.) - 1.95) + 0.002 / (
                          pow_temp(lam / 9.7, 2.) + pow_temp(9.7 / lam, 2.) - 1.95) + 0.002 / (
                          pow_temp(lam / 18., 2.) + pow_temp(18. / lam, 2.) - 1.80) + 0.012 / (
                          pow_temp(lam / 25., 2.) + pow_temp(25. / lam, 2.) + 0.)
        flux = flux / pow_temp(10, 0.4 * 1.32 * Av * extinct)

    elif law is 1:
        extinct = 175. / (pow_temp(lam / 0.046, 2.) + pow_temp(0.046 / lam, 2.) + 90.) + 19. / (
                pow_temp(lam / 0.08, 4.5) + pow_temp(0.08 / lam, 4.5) + 5.50) + 0.023 / (
                          pow_temp(lam / 0.22, 2.) + pow_temp(0.22 / lam, 2.) - 1.95) + 0.005 / (
                          pow_temp(lam / 9.7, 2.) + pow_temp(9.7 / lam, 2.) - 1.95) + 0.006 / (
                          pow_temp(lam / 18., 2.) + pow_temp(18. / lam, 2.) - 1.80) + 0.020 / (
                          pow_temp(lam / 25., 2.) + pow_temp(25. / lam, 2.) + 0.)
        flux = flux / pow_temp(10, 0.4 * 1.34 * Av * extinct)

    elif law is 2:
        extinct = 185. / (pow_temp(lam / 0.042, 2.) + pow_temp(0.042 / lam, 2.) + 90.) + 27. / (
                pow_temp(lam / 0.08, 4.0) + pow_temp(0.08 / lam, 4.0) + 5.50) + 0.005 / (
                          pow_temp(lam / 0.22, 2.) + pow_temp(0.22 / lam, 2.) - 1.95) + 0.010 / (
                          pow_temp(lam / 9.7, 2.) + pow_temp(9.7 / lam, 2.) - 1.95) + 0.012 / (
                          pow_temp(lam / 18., 2.) + pow_temp(18. / lam, 2.) - 1.80) + 0.030 / (
                          pow_temp(lam / 25., 2.) + pow_temp(25. / lam, 2.) + 0.)
        flux = flux / pow_temp(10, 0.4 * 1.32 * Av * extinct)

    elif law is 3:
        x = 1 / lam
        if hasattr(x, '__len__'):
            a = 0
            b = 0
            for i, _x in enumerate(x):
                if 0.2 < _x <= 1.1:
                    a = 0.574 * pow_temp(_x, 1.61)
                    b = -0.527 * pow_temp(_x, 1.61)
                elif 1.1 < _x <= 3.3:
                    y = _x - 1.81
                    a = 1 + 0.17699 * y - 0.50447 * y * y - 0.02427 * pow_temp(y, 3) + 0.72085 * pow_temp(y,
                                                                                                          4) + 0.01979 * pow_temp(
                        y, 5) - 0.77530 * pow_temp(y, 6) + 0.32999 * pow_temp(y, 7)
                    b = 1.41338 * y + 2.28305 * y * y + 1.07233 * pow_temp(y, 3) - 5.38434 * pow_temp(y,
                                                                                                      4) - 0.62251 * pow_temp(
                        y,
                        5) + \
                        5.30260 * pow_temp(y, 6) - 2.09002 * pow_temp(y, 7)
                elif 3.3 < _x < 8.0:
                    if _x < 5.9:
                        Fa = 0
                        Fb = 0
                    else:
                        Fa = -0.04473 * pow_temp(_x - 5.9, 2) - 0.009779 * pow_temp(_x - 5.9, 3)
                        Fb = 0.2130 * pow_temp(_x - 5.9, 2) + 0.1207 * pow_temp(_x - 5.9, 3)

                    a = 1.752 - 0.316 * _x - 0.104 / (pow_temp(_x - 4.67, 2) + 0.341) + Fa
                    b = -3.090 + 1.825 * _x + 1.206 / (pow_temp(_x - 4.62, 2) + 0.263) + Fb

                Alam = Av * (a + b / 3.1)
                flux[i] = flux[i] / pow_temp(10, 0.4 * Alam)
        else:
            if 0.2 < x <= 1.1:
                a = 0.574 * pow_temp(x, 1.61)
                b = -0.527 * pow_temp(x, 1.61)
            elif 1.1 < x <= 3.3:
                y = x - 1.81
                a = 1 + 0.17699 * y - 0.50447 * y * y - 0.02427 * pow_temp(y, 3) + 0.72085 * pow_temp(y,
                                                                                                      4) + 0.01979 * pow_temp(
                    y, 5) - 0.77530 * pow_temp(y, 6) + 0.32999 * pow_temp(y, 7)
                b = 1.41338 * y + 2.28305 * y * y + 1.07233 * pow_temp(y, 3) - 5.38434 * pow_temp(y,
                                                                                                  4) - 0.62251 * pow_temp(
                    y,
                    5) + \
                    5.30260 * pow_temp(y, 6) - 2.09002 * pow_temp(y, 7)
            elif 3.3 < x < 8.0:
                if x < 5.9:
                    Fa = 0
                    Fb = 0
                else:
                    Fa = -0.04473 * pow_temp(x - 5.9, 2) - 0.009779 * pow_temp(x - 5.9, 3)
                    Fb = 0.2130 * pow_temp(x - 5.9, 2) + 0.1207 * pow_temp(x - 5.9, 3)

                a = 1.752 - 0.316 * x - 0.104 / (pow_temp(x - 4.67, 2) + 0.341) + Fa
                b = -3.090 + 1.825 * x + 1.206 / (pow_temp(x - 4.62, 2) + 0.263) + Fb

            Alam = Av * (a + b / 3.1)
            flux = flux / pow_temp(10, 0.4 * Alam)

    elif law is 4:
        extin = np.zeros(len(lam))
        mask = lam < 0.63
        extin[mask] = 2.659 * (-2.156 + 1.509 / lam[mask] - 0.198 / pow_temp(lam[mask], 2.0) + 0.011 / pow_temp(
            lam[mask], 3.0)) + 4.05
        mask = np.invert(mask)
        extin[mask] = 2.659 * (-1.857 + 1.040 / lam[mask]) + 4.05
        flux = flux / pow_temp(10, 0.4 * extin * Av / 4.05)
    return flux
