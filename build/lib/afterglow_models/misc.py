import numpy as np
from afterglow_models.sf_flux_legacy import simulation_templates


def group(unique, accuracy=0.03):
    sorted_unique = sorted(unique)
    result = []
    for i in range(len(sorted_unique)):
        exists = False
        for j in range(len(result)):
            if accuracy < 1.0:
                if sorted_unique[i] * (1 - accuracy) <= result[j] <= sorted_unique[i] * (1 + accuracy):
                    exists = True
            else:
                if sorted_unique[i] / accuracy <= result[j] <= sorted_unique[i] * accuracy:
                    exists = True
        if not exists:
            result.append(sorted_unique[i])
    return np.array(result)


def get_mask(df, val, accuracy=0.03, key='Freq'):
    if accuracy < 1.0:
        return (df[key] >= val * (1 - accuracy)) & (df[key] <= val * (1 + accuracy))
    else:
        return get_mask_wide(df, val, accuracy=accuracy, key=key)


def get_mask_wide(df, val, accuracy=0.03, key='Freq'):
    return (df[key] >= val / accuracy) & (df[key] <= val * accuracy)


def get_upperlimit_mask(df):
    return (df['Flux'] - df['Error'] <= 0) | (df['Flux'] + df['Error'] <= 0) | (df['Error'] == 0)


def get_detection_mask(df):
    return (df['Flux'] - df['Error'] > 0) & (df['Flux'] + df['Error'] > 0) & (df['Error'] != 0)


def in_range(test_val, val, accuracy=0.03):
    return (test_val >= val * (1 - accuracy)) & (test_val <= val * (1 + accuracy))


def convert_boxfit_to_scalefit(params=None, theta_0=None, e_k_iso=None, n=None,
                               theta_obs=None, p=None, eps_b=None, eps_e=None, xi_n=None, z=None, d_l=None):
    res = np.zeros(simulation_templates.no_pars)
    if params is None:
        res[simulation_templates.index_theta_0_] = theta_0
        res[simulation_templates.index_log10_E_iso_53_] = np.log10(e_k_iso / 1e53)
        res[simulation_templates.index_log10_n_ref_] = np.log10(n)
        res[simulation_templates.index_theta_obs_over_theta_0_] = theta_obs / theta_0
        res[simulation_templates.index_p_] = p
        res[simulation_templates.index_log10_eps_B_] = np.log10(eps_b)
        res[simulation_templates.index_log10_eps_e_bar_] = np.log10(eps_e * (p - 2.) / (p - 1))
        res[simulation_templates.index_log10_xi_N_] = np.log10(xi_n)
        res[simulation_templates.index_z_] = z
        res[simulation_templates.index_log10_d_L_28_] = np.log10(d_l / 1e28)
    else:
        res[simulation_templates.index_theta_0_] = params[0]
        res[simulation_templates.index_log10_E_iso_53_] = np.log10(params[1] / 1e53)
        res[simulation_templates.index_log10_n_ref_] = np.log10(params[2])
        res[simulation_templates.index_theta_obs_over_theta_0_] = params[3] / params[0]
        res[simulation_templates.index_p_] = params[4]
        res[simulation_templates.index_log10_eps_B_] = np.log10(params[5])
        res[simulation_templates.index_log10_eps_e_bar_] = np.log10(params[6] * (params[4] - 2.) / (params[4] - 1))
        res[simulation_templates.index_log10_xi_N_] = np.log10(params[7])
        res[simulation_templates.index_z_] = z
        res[simulation_templates.index_log10_d_L_28_] = np.log10(d_l / 1e28)
    return res
