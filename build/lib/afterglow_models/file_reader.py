import pandas as pn

from . import misc


def read_obs_data(data_filename, accuracy=0.03):
    df = pn.read_csv(data_filename, delimiter=',', comment='#', header=None, names=['Time', 'Freq', 'Flux', 'Error'])
    bands = misc.group(df['Freq'].unique(), accuracy=accuracy)
    return df, bands


def read_boxfit_output(data_filename, accuracy=0.03):
    df = pn.read_csv(data_filename, delimiter=',', comment='#', header=None,
                     names=['Index', 'Time', 'Freq', 'Flux'])
    return df


def read_scalefit_output(data_filename, accuracy=0.03):
    df = pn.read_csv(data_filename, delimiter=',', comment='#', header=None,
                     names=['Index', 'Time', 'Freq', 'Flux'])
    return df


def read_scalefit_gp_output(data_filename, accuracy=0.03):
    df = pn.read_csv(data_filename, delimiter=',', comment='#', header=None,
                     names=['Index', 'Time', 'Freq', 'Flux', 'Mean', 'Sigma_low', 'Sigma_up', 'Likelihood', 'Chi2'])
    return df
