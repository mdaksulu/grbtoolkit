import fileinput
import io
import os
import subprocess
import sys
import time as imported_time

from . import boxfit_reader
from . import file_reader
from . import misc

boxfile_ISM_path = '"/Users/mda/bin/boxfiles/ISM/boxISM_"'
boxfile_WIND_path = '"/Users/mda/bin/boxfiles/WIND/boxboostmedwind_"'

boxfit_executable_ISM = "boxfit"
boxfit_executable_WIND = "boxfit-boost"


def generate_light_curve(parameters, t_0, t_1, freq, wind=False, boxfit_settings_filename='boxfitsettings.txt',
                         output_filename='lc.txt', no_points=100, z=None, d_l=None, boxfit_exe=None, t_inj=None,
                         dt_inj=None, alpha=None):
    boxfile_path = boxfile_ISM_path
    boxfit_executable = boxfit_executable_ISM
    if wind:
        boxfile_path = boxfile_WIND_path
        boxfit_executable = boxfit_executable_WIND
    if boxfit_exe is not None:
        boxfit_executable = boxfit_exe
    if not os.path.isfile(output_filename):
        f = fileinput.FileInput(boxfit_settings_filename, inplace=True)
        for line in f:
            if line.strip().startswith('what_to_do = '):
                line = " what_to_do = 1\n"
            if line.strip().startswith('box_filename_base = '):
                line = " box_filename_base = {0}\n".format(boxfile_path)
            if line.strip().startswith('no_points = '):
                line = " no_points = {0}\n".format(no_points)
            if line.strip().startswith('t_0 = '):
                line = " t_0 = {:10E}\n".format(t_0)
            if line.strip().startswith('t_1 = '):
                line = " t_1 = {:10E}\n".format(t_1)
            if line.strip().startswith('nu_0 = '):
                line = " nu_0 = {:10E}\n".format(freq)
            if line.strip().startswith('theta_0 = '):
                line = " theta_0 = {:10E}\n".format(parameters[0])
            if line.strip().startswith('E = '):
                line = " E = {:10E}\n".format(parameters[1])
            if line.strip().startswith('n = '):
                line = " n = {:10E}\n".format(parameters[2])
            if line.strip().startswith('theta_obs = '):
                line = " theta_obs = {:10E}\n".format(parameters[3])
            if line.strip().startswith('p = '):
                line = " p = {:10E}\n".format(parameters[4])
            if line.strip().startswith('epsilon_B = '):
                line = " epsilon_B = {:10E}\n".format(parameters[5])
            if line.strip().startswith('epsilon_E = '):
                line = " epsilon_E = {:10E}\n".format(parameters[6])
            if line.strip().startswith('ksi_N = '):
                line = " ksi_N = {:10E}\n".format(parameters[7])
            if z is not None and line.strip().startswith('z = '):
                line = " z = {:10E}\n".format(z)
            if d_l is not None and line.strip().startswith('d_L = '):
                line = " d_L = {:10E}\n".format(d_l)
            if t_inj is not None and line.strip().startswith('t_inj = '):
                line = " t_inj = {:10E}\n".format(t_inj)
            if dt_inj is not None and line.strip().startswith('dt_inj = '):
                line = " dt_inj = {:10E}\n".format(dt_inj)
            if alpha is not None and line.strip().startswith('alpha = '):
                line = " alpha = {:10E}\n".format(alpha)
            sys.stdout.write(line)
        f.close()
        log_file_name = 'log.txt'
        cmd = ['mpirun', boxfit_executable]
        with io.open(log_file_name, 'wb') as writer, io.open(log_file_name, 'rb', 1) as reader:
            process = subprocess.Popen(cmd, stdout=writer)
            while process.poll() is None:
                sys.stdout.write(reader.read().decode('utf-8'))
                imported_time.sleep(0.5)
            sys.stdout.write(reader.read().decode('utf-8'))
        if len(parameters) > 8:
            host_flux = 0
            host_bands, band_margin = boxfit_reader.read_host_bands(boxfit_settings_filename=boxfit_settings_filename)
            for i, band in enumerate(host_bands):
                if misc.in_range(band, freq, accuracy=band_margin):
                    host_flux = parameters[8 + i]
                    break
            df_lc = file_reader.read_boxfit_output('lightcurve.txt', accuracy=band_margin)
            time = df_lc['Time'].values
            flux = df_lc['Flux'].values
            flux = flux + host_flux
            f = open(output_filename, 'w')
            for i, t in enumerate(time):
                f.write('{:d}, {:10e}, {:10e}, {:10e}\n'.format(i, t, freq, flux[i]))
            f.close()
            if output_filename != 'lightcurve.txt':
                os.remove('lightcurve.txt')
        else:
            os.rename('lightcurve.txt', output_filename)


def generate_spectrum(parameters, nu_0, nu_1, time, wind=False, boxfit_settings_filename='boxfitsettings.txt',
                      output_filename='spec.txt', no_points=100, ssa=1, cooling=1):
    boxfile_path = boxfile_ISM_path
    boxfit_executable = boxfit_executable_ISM
    if wind:
        boxfile_path = boxfile_WIND_path
        boxfit_executable = boxfit_executable_WIND
    if not os.path.isfile(output_filename):
        f = fileinput.FileInput(boxfit_settings_filename, inplace=True)
        for line in f:
            if line.strip().startswith('what_to_do = '):
                line = " what_to_do = 2\n"
            if line.strip().startswith('box_filename_base = '):
                line = " box_filename_base = {0}\n".format(boxfile_path)
            if line.strip().startswith('no_points = '):
                line = " no_points = {0}\n".format(no_points)
            if line.strip().startswith('nu_0 = '):
                line = " nu_0 = {:10E}\n".format(nu_0)
            if line.strip().startswith('nu_1 = '):
                line = " nu_1 = {:10E}\n".format(nu_1)
            if line.strip().startswith('t_0 = '):
                line = " t_0 = {:10E}\n".format(time)
            if line.strip().startswith('theta_0 = '):
                line = " theta_0 = {:10E}\n".format(parameters[0])
            if line.strip().startswith('E = '):
                line = " E = {:10E}\n".format(parameters[1])
            if line.strip().startswith('n = '):
                line = " n = {:10E}\n".format(parameters[2])
            if line.strip().startswith('theta_obs = '):
                line = " theta_obs = {:10E}\n".format(parameters[3])
            if line.strip().startswith('p = '):
                line = " p = {:10E}\n".format(parameters[4])
            if line.strip().startswith('epsilon_B = '):
                line = " epsilon_B = {:10E}\n".format(parameters[5])
            if line.strip().startswith('epsilon_E = '):
                line = " epsilon_E = {:10E}\n".format(parameters[6])
            if line.strip().startswith('ksi_N = '):
                line = " ksi_N = {:10E}\n".format(parameters[7])
            if line.strip().startswith('self_absorption = '):
                line = " self_absorption = {:d}\n".format(ssa)
            if line.strip().startswith('electron_cooling = '):
                line = " electron_cooling = {:d}\n".format(cooling)
            sys.stdout.write(line)
        f.close()
        log_file_name = 'log.txt'
        cmd = ['mpirun', boxfit_executable]
        with io.open(log_file_name, 'wb') as writer, io.open(log_file_name, 'rb', 1) as reader:
            process = subprocess.Popen(cmd, stdout=writer)
            while process.poll() is None:
                sys.stdout.write(reader.read().decode('utf-8'))
                imported_time.sleep(0.5)
            # Read the remaining
            sys.stdout.write(reader.read().decode('utf-8'))

        os.rename('spectrum.txt', output_filename)
