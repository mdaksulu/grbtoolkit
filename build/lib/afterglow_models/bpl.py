import numpy as np


class BPL:
    def __init__(self, norm=None, x_b=None, beta=None, s_b=None):
        if norm is not None:
            self.norm = norm
        else:
            self.norm = None
        if x_b is not None:
            self.x_b = np.array(x_b)
            self.n_breaks = len(x_b)
        else:
            self.x_b = None
            self.n_breaks = None
        if beta is not None:
            self.beta = np.array(beta)
        else:
            self.beta = None
        if s_b is not None:
            self.s_b = np.array(s_b)
        else:
            self.s_b = None

    def __str__(self):
        return 'It\'s working.'

    def set_parameters(self, norm=None, x_b=None, beta=None, s_b=None):
        self.n_breaks = len(x_b)
        if norm is not None:
            self.norm = norm
        if x_b is not None:
            self.x_b = np.array(x_b)
        if beta is not None:
            self.beta = np.array(beta)
        if s_b is not None:
            self.s_b = np.array(s_b)

    def evaluate(self, x):
        x_arr = np.array(x)
        res = np.full(len(x_arr), self.norm, dtype=float)
        n_segments = self.n_breaks + 1
        if self.s_b is None:
            for i in range(n_segments):
                if i == 0:
                    mask = x_arr <= self.x_b[0]
                    res[mask] = res[mask] * (x_arr[mask] / self.x_b[0]) ** self.beta[0]
                elif i == 1:
                    mask = (self.x_b[0] < x_arr) & (x_arr <= self.x_b[1])
                    res[mask] = res[mask] * (x_arr[mask] / self.x_b[0]) ** self.beta[1]
                elif i == 2:
                    if n_segments == 4:
                        mask = (self.x_b[1] < x_arr) & (x <= self.x_b[2])
                    elif n_segments == 3:
                        mask = (self.x_b[1] < x_arr)
                    res[mask] = res[mask] * (self.x_b[1] / self.x_b[0]) ** self.beta[1]
                    res[mask] = res[mask] * (x_arr[mask] / self.x_b[1]) ** self.beta[2]
                elif i == 3:
                    mask = self.x_b[2] < x_arr
                    res[mask] = res[mask] * ((self.x_b[1] / self.x_b[0]) ** self.beta[1] *
                                             (self.x_b[2] / self.x_b[1]) ** self.beta[2])
                    res[mask] = res[mask] * (x_arr[mask] / self.x_b[2]) ** self.beta[3]
            return res
        else:
            res = self.norm * ((x_arr / self.x_b[0]) ** (-self.s_b[0] * self.beta[0]) +
                               (x_arr / self.x_b[0]) ** (-self.s_b[0] * self.beta[1])) ** (-1. / self.s_b[0])
            for i in range(1, self.n_breaks):
                res = res * (1 + (x_arr / self.x_b[i]) **
                             (self.s_b[i] * (self.beta[i] - self.beta[i + 1]))) ** (-1. / self.s_b[i])
            return res
