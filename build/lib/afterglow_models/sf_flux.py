from afterglow_models.sf_flux_legacy import simulation_templates
from afterglow_data.misc import convert_hz_to_a
from afterglow_data.misc import in_range
from afterglow_data.extinction import application
import numpy as np
import george
from george import kernels
from scipy.optimize import minimize

TINY_FLUX = 1e-30
TINY_LIKELIHOOD = -1e300
HUGE_CHI2 = 1e300


class Scalefit:
    def __init__(self, wind=False):
        self.table_filename = '/Users/mda/bin/scalefit-tables/tableISM.h5'
        if wind:
            self.table_filename = '/Users/mda/bin/scalefit-tables/tableWIND.h5'
        self.sf = simulation_templates(table=self.table_filename)
        pass

    def f_nu(self, sf_params, t_obs, nu_obs,
             host_type=None,
             a_v_host=None,
             persistent_bands=None,
             persistent_flux=None):
        f_nu_arr = np.array(self.sf.Fnu(params=sf_params, times=t_obs, nu=nu_obs))
        # Apply extinction law
        if host_type is not None and a_v_host is not None:
            z = sf_params[self.sf.index_z_]
            lambda_rest = convert_hz_to_a(nu_obs) * (1 + z)
            f_nu_arr = application(lam=lambda_rest, flux=f_nu_arr, Av=a_v_host, law=host_type)
        # Add persistent flux
        if persistent_bands is not None and persistent_flux is not None:
            for b, band in enumerate(persistent_bands):
                b_mask = in_range(nu_obs, band)
                f_nu_arr[b_mask] = f_nu_arr[b_mask] + persistent_flux[b]
        return f_nu_arr

    def f_nu_gp(self, t, nu, sf_params, log10_hyperparameters, f_nu_obs, err_obs, t_obs, nu_obs,
                host_type=None,
                a_v_host=None,
                persistent_bands=None,
                persistent_flux=None,
                log10_t=True,
                log10_nu=True,
                log10_f=True,
                use_error=True,
                use_frac_error=False,
                use_upper_err=False,
                t_kernel_type='exp2',
                f_kernel_type='exp2',
                poly_order=4):
        a, l1, l2, white_noise, p = log10_hyperparameters

        k = 10 ** a
        if t_kernel_type == 'exp2':
            k = k * kernels.ExpSquaredKernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'matern32':
            k = k * kernels.Matern32Kernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'matern52':
            k = k * kernels.Matern52Kernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'poly':
            k = k * kernels.PolynomialKernel(log_sigma2=np.log(10 ** l1), ndim=2, axes=0, order=poly_order)
        elif t_kernel_type == 'lin':
            k = k * kernels.LinearKernel(log_gamma2=np.log(10 ** l1), ndim=2, axes=0, order=poly_order)
        elif t_kernel_type == 'cos':
            k = k * kernels.CosineKernel(log_period=np.log(10 ** l1), ndim=2, axes=0)
        elif t_kernel_type == 'cos-exp2':
            k = k * (kernels.CosineKernel(log_period=np.log(10 ** p), ndim=2, axes=0) *
                     kernels.ExpSquaredKernel(10 ** l1, ndim=2, axes=0))

        if f_kernel_type == 'exp2':
            k = k * kernels.ExpSquaredKernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'matern32':
            k = k * kernels.Matern32Kernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'matern52':
            k = k * kernels.Matern52Kernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'poly':
            k = k * kernels.PolynomialKernel(log_sigma2=np.log(10 ** l2), ndim=2, axes=1, order=poly_order)
        elif f_kernel_type == 'lin':
            k = k * kernels.LinearKernel(log_gamma2=np.log(10 ** l2), ndim=2, axes=1, order=poly_order)
        elif f_kernel_type == 'cos':
            k = k * kernels.CosineKernel(log_period=np.log(10 ** l2), ndim=2, axes=1)
        elif f_kernel_type == 'cos-exp2':
            k = k * (kernels.CosineKernel(log_period=np.log(10 ** p), ndim=2, axes=1) *
                     kernels.ExpSquaredKernel(10 ** l2, ndim=2, axes=1))

        gp = george.GP(k)
        f_nu = self.f_nu(sf_params=sf_params, t_obs=t_obs, nu_obs=nu_obs, host_type=host_type,
                         a_v_host=a_v_host, persistent_bands=persistent_bands, persistent_flux=persistent_flux)
        f_nu_obs_norm = self.normalize_flux(f_nu_obs, f_nu_obs=f_nu_obs, nu_obs=nu_obs)
        err_obs_norm = self.normalize_flux(err_obs, f_nu_obs=f_nu_obs, nu_obs=nu_obs)
        f_nu_norm = self.normalize_flux(f_nu, f_nu_obs=f_nu_obs, nu_obs=nu_obs)
        # print(f_nu_obs)
        if log10_f:
            f_nu_norm[f_nu_norm == 0] = TINY_FLUX
            log10_f_nu_norm = np.log10(f_nu_norm)
            log10_f_nu_obs_norm = np.log10(f_nu_obs_norm)
            log10_err_obs_norm = log10_f_nu_obs_norm - np.log10(f_nu_obs_norm - err_obs_norm)
            if use_upper_err:
                log10_err_obs_norm = np.log10(f_nu_obs_norm + err_obs_norm) - log10_f_nu_obs_norm
            y = log10_f_nu_obs_norm - log10_f_nu_norm

        else:
            y = f_nu_obs_norm - f_nu_norm
        if log10_t:
            x1 = np.log10(t_obs)
        else:
            x1 = t_obs
        if log10_nu:
            x2 = np.log10(nu_obs)
        else:
            x2 = nu_obs
        x_2d = np.column_stack((x1, x2))
        if use_error:
            if log10_f:
                gp.compute(x_2d, log10_err_obs_norm * 10 ** white_noise)
            else:
                gp.compute(x_2d, err_obs_norm * 10 ** white_noise)
        else:
            if log10_f:
                gp.compute(x_2d, log10_f_nu_norm * 10 ** white_noise)
            else:
                gp.compute(x_2d, f_nu_norm * 10 ** white_noise)

        if log10_t:
            x1_model = np.log10(t)
        else:
            x1_model = t
        if log10_nu:
            x2_model = np.log10(nu)
        else:
            x2_model = nu

        mean, cov = gp.predict(y, x_2d, return_var=True)
        x_2d_model = np.column_stack((x1_model, x2_model))
        mean, cov = gp.predict(y, x_2d_model)
        print(cov)

        f_nu = self.f_nu(sf_params=sf_params, t_obs=t, nu_obs=nu, host_type=host_type,
                         a_v_host=a_v_host, persistent_bands=persistent_bands, persistent_flux=persistent_flux)
        f_nu_norm = self.normalize_flux(f_nu, f_nu_obs=f_nu_obs, nu_obs=nu_obs, nu=nu)
        f_nu_norm[f_nu_norm == 0] = TINY_FLUX
        if use_error:
            if log10_f:
                std = np.sqrt(np.diag(cov) + (log10_err_obs_norm * 10 ** white_noise) ** 2)
            else:
                std = np.sqrt(np.diag(cov) + (err_obs_norm * 10 ** white_noise) ** 2)
        else:
            if log10_f:
                log10_f_nu_norm = np.log10(f_nu_norm)
                std = np.sqrt(np.diag(cov) + (log10_f_nu_norm * 10 ** white_noise) ** 2)
            else:
                std = np.sqrt(np.diag(cov) + (f_nu_norm * 10 ** white_noise) ** 2)

        if log10_f:
            mean_model = self.denormalize_flux(10 ** (np.log10(f_nu_norm) + mean), nu_obs=nu_obs, f_nu_obs=f_nu_obs, nu=nu)
            sigma_up_model = self.denormalize_flux(10 ** (np.log10(f_nu_norm) + mean + std), nu_obs=nu_obs, f_nu_obs=f_nu_obs, nu=nu)
            sigma_low_model = self.denormalize_flux(10 ** (np.log10(f_nu_norm) + mean - std), nu_obs=nu_obs, f_nu_obs=f_nu_obs, nu=nu)
        else:
            mean_model = self.denormalize_flux(f_nu_norm + mean, nu_obs=nu_obs, f_nu_obs=f_nu_obs, nu=nu)
            sigma_up_model = self.denormalize_flux(f_nu_norm + mean + std, nu_obs=nu_obs, f_nu_obs=f_nu_obs, nu=nu)
            sigma_low_model = self.denormalize_flux(f_nu_norm + mean - std, nu_obs=nu_obs, f_nu_obs=f_nu_obs, nu=nu)
        # print(f_nu)
        # print(mean)
        # print(std)
        # print(y)
        # print(f_nu_norm)
        # print(mean_model)
        # print(sigma_up_model)
        # print(sigma_low_model)
        # print(f_nu_obs_norm)
        return mean_model, sigma_up_model, sigma_low_model

    def gp_lnlikelihood(self, sf_params, log10_hyperparameters, f_nu_obs, err_obs, t_obs, nu_obs,
                        host_type=None,
                        a_v_host=None,
                        persistent_bands=None,
                        persistent_flux=None,
                        log10_t=True,
                        log10_nu=True,
                        log10_f=True,
                        use_error=True,
                        use_upper_err=False,
                        use_frac_error=False,
                        t_kernel_type='exp2',
                        f_kernel_type='exp2',
                        poly_order=4):
        a, l1, l2, white_noise, p = log10_hyperparameters

        k = 10 ** a
        if t_kernel_type == 'exp2':
            k = k * kernels.ExpSquaredKernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'matern32':
            k = k * kernels.Matern32Kernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'matern52':
            k = k * kernels.Matern52Kernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'poly':
            k = k * kernels.PolynomialKernel(log_sigma2=np.log(10 ** l1), ndim=2, axes=0, order=poly_order)
        elif t_kernel_type == 'lin':
            k = k * kernels.LinearKernel(log_gamma2=np.log(10 ** l1), ndim=2, axes=0, order=poly_order)
        elif t_kernel_type == 'cos':
            k = k * kernels.CosineKernel(log_period=np.log(10 ** l1), ndim=2, axes=0)
        elif t_kernel_type == 'cos-exp2':
            k = k * (kernels.CosineKernel(log_period=np.log(10 ** p), ndim=2, axes=0) *
                     kernels.ExpSquaredKernel(10 ** l1, ndim=2, axes=0))

        if f_kernel_type == 'exp2':
            k = k * kernels.ExpSquaredKernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'matern32':
            k = k * kernels.Matern32Kernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'matern52':
            k = k * kernels.Matern52Kernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'poly':
            k = k * kernels.PolynomialKernel(log_sigma2=np.log(10 ** l2), ndim=2, axes=1, order=poly_order)
        elif f_kernel_type == 'lin':
            k = k * kernels.LinearKernel(log_gamma2=np.log(10 ** l2), ndim=2, axes=1, order=poly_order)
        elif f_kernel_type == 'cos':
            k = k * kernels.CosineKernel(log_period=np.log(10 ** l2), ndim=2, axes=1)
        elif f_kernel_type == 'cos-exp2':
            k = k * (kernels.CosineKernel(log_period=np.log(10 ** p), ndim=2, axes=1) *
                     kernels.ExpSquaredKernel(10 ** l2, ndim=2, axes=1))

        gp = george.GP(k)
        f_nu = self.f_nu(sf_params=sf_params, t_obs=t_obs, nu_obs=nu_obs, host_type=host_type,
                         a_v_host=a_v_host, persistent_bands=persistent_bands, persistent_flux=persistent_flux)
        f_nu_obs_norm = self.normalize_flux(f_nu_obs, f_nu_obs=f_nu_obs, nu_obs=nu_obs)
        err_obs_norm = self.normalize_flux(err_obs, f_nu_obs=f_nu_obs, nu_obs=nu_obs)
        f_nu_norm = self.normalize_flux(f_nu, f_nu_obs=f_nu_obs, nu_obs=nu_obs)
        # print(err_obs)
        if log10_f:
            f_nu_norm[f_nu_norm == 0] = TINY_FLUX
            log10_f_nu_norm = np.log10(f_nu_norm)
            log10_f_nu_obs_norm = np.log10(f_nu_obs_norm)
            log10_err_obs_norm = log10_f_nu_obs_norm - np.log10(f_nu_obs_norm - err_obs_norm)
            if use_upper_err:
                log10_err_obs_norm = np.log10(f_nu_obs + err_obs) - log10_f_nu_obs_norm
            y = log10_f_nu_obs_norm - log10_f_nu_norm

        else:
            y = f_nu_obs_norm - f_nu_norm
        if np.any(np.isinf(y)) or np.any(np.isnan(y)):
            return TINY_LIKELIHOOD
        if log10_t:
            x1 = np.log10(t_obs)
        else:
            x1 = t_obs
        if log10_nu:
            x2 = np.log10(nu_obs)
        else:
            x2 = nu_obs
        x_2d = np.column_stack((x1, x2))
        try:
            if use_error:
                if log10_f:
                    gp.compute(x_2d, log10_err_obs_norm * 10 ** white_noise)
                else:
                    gp.compute(x_2d, err_obs_norm * 10 ** white_noise)
            else:
                if log10_f:
                    gp.compute(x_2d, log10_f_nu_norm * 10 ** white_noise)
                else:
                    gp.compute(x_2d, f_nu_norm * 10 ** white_noise)
        except np.linalg.LinAlgError:
            return TINY_LIKELIHOOD
        try:
            likelihood = gp.log_likelihood(y, quiet=True)
        except np.linalg.LinAlgError:
            return TINY_LIKELIHOOD
        return likelihood

    def gp_lnlikelihood_marginalized(self, sf_params, f_nu_obs, err_obs, t_obs, nu_obs,
                                     host_type=None,
                                     a_v_host=None,
                                     persistent_bands=None,
                                     persistent_flux=None,
                                     log10_t=True,
                                     log10_nu=True,
                                     log10_f=True,
                                     use_error=True,
                                     use_frac_error=False,
                                     use_upper_err=False,
                                     t_kernel_type='exp2',
                                     f_kernel_type='exp2',
                                     poly_order=4):

        f_nu = self.f_nu(sf_params=sf_params, t_obs=t_obs, nu_obs=nu_obs, host_type=host_type,
                         a_v_host=a_v_host, persistent_bands=persistent_bands, persistent_flux=persistent_flux)
        if log10_f:
            f_nu[f_nu == 0] = TINY_FLUX
            log10_f_nu = np.log10(f_nu)
            log10_f_nu_obs = np.log10(f_nu_obs)
            log10_err_obs = log10_f_nu_obs - np.log10(f_nu_obs - err_obs)
            if use_upper_err:
                log10_err_obs = np.log10(f_nu_obs + err_obs) - log10_f_nu_obs
            if use_frac_error:
                y = (log10_f_nu_obs - log10_f_nu) / log10_f_nu_obs
                log10_err_obs = log10_err_obs / log10_f_nu_obs
            else:
                y = log10_f_nu_obs - log10_f_nu

        else:
            if use_frac_error:
                y = (f_nu_obs - f_nu) / f_nu_obs
                err_obs = err_obs / f_nu_obs
            else:
                y = f_nu_obs - f_nu
        if np.any(np.isinf(y)) or np.any(np.isnan(y)):
            return TINY_LIKELIHOOD
        if log10_t:
            x1 = np.log10(t_obs)
        else:
            x1 = t_obs
        if log10_nu:
            x2 = np.log10(nu_obs)
        else:
            x2 = nu_obs
        x_2d = np.column_stack((x1, x2))

        a, l1, l2, white_noise, p = (np.log10(np.var(y)),
                                     np.log10(max(x1) - min(x1)),
                                     np.log10(max(x2) - min(x2)),
                                     np.log10(1.), np.log10(max(x1) - min(x1)))

        k = 10 ** a
        if t_kernel_type == 'exp2':
            k = k * kernels.ExpSquaredKernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'matern32':
            k = k * kernels.Matern32Kernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'matern52':
            k = k * kernels.Matern52Kernel(10 ** l1, ndim=2, axes=0)
        elif t_kernel_type == 'poly':
            k = k * kernels.PolynomialKernel(log_sigma2=np.log(10 ** l1), ndim=2, axes=0, order=poly_order)
        elif t_kernel_type == 'lin':
            k = k * kernels.LinearKernel(log_gamma2=np.log(10 ** l1), ndim=2, axes=0, order=poly_order)
        elif t_kernel_type == 'cos':
            k = k * kernels.CosineKernel(log_period=np.log(10 ** l1), ndim=2, axes=0)
        elif t_kernel_type == 'cos-exp2':
            k = k * (kernels.CosineKernel(log_period=np.log(10 ** p), ndim=2, axes=0) *
                     kernels.ExpSquaredKernel(10 ** l1, ndim=2, axes=0))

        if f_kernel_type == 'exp2':
            k = k * kernels.ExpSquaredKernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'matern32':
            k = k * kernels.Matern32Kernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'matern52':
            k = k * kernels.Matern52Kernel(10 ** l2, ndim=2, axes=1)
        elif f_kernel_type == 'poly':
            k = k * kernels.PolynomialKernel(log_sigma2=np.log(10 ** l2), ndim=2, axes=1, order=poly_order)
        elif f_kernel_type == 'lin':
            k = k * kernels.LinearKernel(log_gamma2=np.log(10 ** l2), ndim=2, axes=1, order=poly_order)
        elif f_kernel_type == 'cos':
            k = k * kernels.CosineKernel(log_period=np.log(10 ** l2), ndim=2, axes=1)
        elif f_kernel_type == 'cos-exp2':
            k = k * (kernels.CosineKernel(log_period=np.log(10 ** p), ndim=2, axes=1) *
                     kernels.ExpSquaredKernel(10 ** l2, ndim=2, axes=1))

        gp = george.GP(k)

        try:
            if use_error:
                if log10_f:
                    gp.compute(x_2d, log10_err_obs * 10 ** white_noise)
                else:
                    gp.compute(x_2d, err_obs * 10 ** white_noise)
            else:
                if log10_f:
                    gp.compute(x_2d, log10_f_nu * 10 ** white_noise)
                else:
                    gp.compute(x_2d, f_nu * 10 ** white_noise)
        except np.linalg.LinAlgError:
            return TINY_LIKELIHOOD

        self.gp = gp
        self.y = y
        # print("\nInitial ln-likelihood: {0:.2f} with hyperparameters:".format(self.gp.log_likelihood(self.y)),
        #       self.gp.get_parameter_vector())
        result = minimize(self.neg_ln_like, self.gp.get_parameter_vector(), jac=self.grad_neg_ln_like)
        gp.set_parameter_vector(result.x)
        # print("\nFinal ln-likelihood: {0:.2f} with hyperparameters:".format(self.gp.log_likelihood(self.y)),
        #       self.gp.get_parameter_vector())

        try:
            likelihood = self.gp.log_likelihood(y, quiet=True)
        except np.linalg.LinAlgError:
            return TINY_LIKELIHOOD
        return likelihood

    def neg_ln_like(self, p):
        self.gp.set_parameter_vector(p)
        try:
            res = -self.gp.log_likelihood(self.y)
        except:
            res = np.inf
        return res

    def grad_neg_ln_like(self, p):
        self.gp.set_parameter_vector(p)
        try:
            res = -self.gp.grad_log_likelihood(self.y)
        except:
            res = np.inf
        return res

    def normalize_flux(self, f_nu, f_nu_obs=None, nu_obs=None, nu=None):
        res = np.zeros(len(f_nu))
        unique_bands = np.unique(nu_obs)
        for band in unique_bands:
            mask = band == nu_obs
            max_flux = max(f_nu_obs[mask])
            if nu is None:
                res[mask] = f_nu[mask] / max_flux
            else:
                mask = band == nu
                res[mask] = f_nu[mask] / max_flux

        return res

    def denormalize_flux(self, f_nu, f_nu_obs, nu_obs, nu=None):
        res = np.zeros(len(f_nu))
        unique_bands = np.unique(nu_obs)
        for band in unique_bands:
            mask = band == nu_obs
            max_flux = max(f_nu_obs[mask])
            if nu is None:
                res[mask] = f_nu[mask] * max_flux
            else:
                mask = band == nu
                res[mask] = f_nu[mask] * max_flux

        return res


    def chi2_lnlikelihood(self, sf_params, f_nu_obs, err_obs, t_obs, nu_obs,
                          host_type=None,
                          a_v_host=None,
                          persistent_bands=None,
                          persistent_flux=None, ):
        f_nu = self.f_nu(sf_params=sf_params, t_obs=t_obs, nu_obs=nu_obs, host_type=host_type,
                         a_v_host=a_v_host, persistent_bands=persistent_bands, persistent_flux=persistent_flux)
        if np.any(np.isinf(f_nu)) or np.any(np.isnan(f_nu)):
            return TINY_LIKELIHOOD
        chi2 = np.sum((f_nu_obs - f_nu) ** 2 / err_obs ** 2)
        return -chi2
