from afterglow_models.bpl import BPL
import numpy as np


class SF_BPL:
    def __init__(self, f_peak, nu_a, nu_a2, nu_a3, nu_a3alt, nu_a4, nu_a5, nu_m, nu_c, p, spec_type=None):
        self.f_peak = f_peak
        self.nu_a = nu_a
        self.nu_a2 = nu_a2
        self.nu_a3 = nu_a3
        self.nu_a3alt = nu_a3alt
        self.nu_a4 = nu_a4
        self.nu_a5 = nu_a5
        self.nu_m = nu_m
        self.nu_c = nu_c
        self.p = p
        self.spec_type = 0
        if spec_type is None:
            if nu_a < nu_m < nu_c:
                self.spec_type = 0
            elif nu_m < nu_a2 < nu_c:
                self.spec_type = 1
            elif nu_m < nu_c < nu_a3:
                self.spec_type = 2
            elif nu_c < nu_m < nu_a3alt:
                self.spec_type = 3
            elif nu_c < nu_a < nu_m:
                self.spec_type = 4
            elif nu_a < nu_c < nu_m:
                self.spec_type = 5
        else:
            self.spec_type = spec_type
        if self.spec_type == 0:
            self.b = np.array([2, 1. / 3., 0.5 * (1. - p), -0.5 * p])
            self.s = np.array([-1.2012 * p + 4.6986, 1.1899, 9.1616 * p + -16.4112])
            self.nu_b = np.array([nu_a, nu_m, nu_c])
        if self.spec_type == 1:
            self.b = np.array([2, 2.5, 0.5 * (1. - p), -0.5 * p])
            self.s = np.array([-0.5153 * p + 0.3405, 0.8363, 9.7571 * p + -17.8512])
            self.nu_b = np.array([nu_m, nu_a2, nu_c])
        if self.spec_type == 2:
            self.b = np.array([2, 0.1089 * p + 2.1660, 0.2176 * p + 1.7532, -0.5 * p])
            self.s = np.array([-2.4585, 1.9640 * p + -2.5320, -1.0335 * p + 3.3760])
            self.nu_b = np.array([nu_m, nu_c, nu_a3])
        if self.spec_type == 3:
            self.b = np.array([2, 11. / 8., 1.8, -0.5 * p])
            self.s = np.array([-0.9807 * p + 3.0119, 1.8807 * p + -6.0652, -0.4450 * p + 2.2568])
            self.nu_b = np.array([nu_c, nu_m, nu_a2])
        if self.spec_type == 4:
            self.b = np.array([2, 1.0787, -0.5, -0.5 * p])
            self.s = np.array([-0.7395 * p + 2.5906, 3.7415 * p + -7.1100, 0.2687 * p + 0.7918])
            self.nu_b = np.array([nu_c, nu_a, nu_m])
        if self.spec_type == 5:
            self.b = np.array([2, 1. / 3., -0.5, -0.5 * p])
            self.s = np.array([-0.5630 * p + 2.4628, 1.7284, -0.3170 * p + 2.2705])
            self.nu_b = np.array([nu_a, nu_c, nu_m])

    def __str__(self):
        pass

    def norm(self):
        if self.spec_type == 0:
            return self.f_peak * (self.nu_a / self.nu_m) ** self.b[1]
        elif self.spec_type == 1:
            return self.f_peak * (self.nu_a2 / self.nu_m) ** (self.b[2] - self.b[1])
        elif self.spec_type == 2:
            # return self.f_peak * (self.nu_a2 / self.nu_m) ** (-0.5 * (1. - self.p) - 5. / 2.)
            return self.f_peak * (self.nu_m / self.nu_a4) ** self.b[1]
        elif self.spec_type == 3:
            # return (self.f_peak * (self.nu_a2 / self.nu_m) ** (1. / 3.) *
            #         (self.nu_m / self.nu_a2) ** 2. *
            #         (self.nu_m / self.nu_c) ** -0.5)
            # return self.f_peak * (self.nu_c / self.nu_a4) ** self.b[0]
            # return self.f_peak * (self.nu_a2 / self.nu_c) ** self.b[3] * (self.nu_m / self.nu_a2) ** self.b[2] *
            # (self.nu_c / self.nu_m) ** self.b[1]
            return self.f_peak * (self.nu_m / self.nu_a) ** self.b[2] * (self.nu_c / self.nu_m) ** self.b[1]
        elif self.spec_type == 4:
            # return self.f_peak * (self.nu_a / self.nu_m) ** -0.5
            return self.f_peak * (self.nu_m / self.nu_c) ** self.b[2] * (self.nu_a / self.nu_m) ** self.b[2] * (self.nu_c / self.nu_a) ** self.b[1]
        elif self.spec_type == 5:
            return self.f_peak * (self.nu_a / self.nu_c) ** self.b[1]

    def evaluate(self, nu, smooth=True, s=None, b=None, nu_b=None):
        if s is not None:
            self.s = s
        if b is not None:
            self.b = b
        if nu_b is not None:
            self.nu_b = nu_b
        if smooth:
            bpl = BPL(norm=self.norm(), x_b=self.nu_b, beta=self.b, s_b=self.s)
        else:
            bpl = BPL(norm=self.norm(), x_b=self.nu_b, beta=self.b)
        return bpl.evaluate(nu)
