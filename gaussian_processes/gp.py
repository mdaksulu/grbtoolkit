import numpy as np
from scipy.linalg import lapack
from scipy.linalg import cholesky

TINY_LIKELIHOOD = -1e300
inds_cache = {}


def upper_triangular_to_symmetric(ut):
    n = ut.shape[0]
    try:
        inds = inds_cache[n]
    except KeyError:
        inds = np.tri(n, k=-1, dtype=np.bool)
        inds_cache[n] = inds
    ut[inds] = ut.T[inds]


def fast_positive_definite_inverse(m):
    cholesky, info = lapack.dpotrf(m)
    if info != 0:
        raise ValueError('dpotrf failed on input {}'.format(m))
    inv, info = lapack.dpotri(cholesky)
    if info != 0:
        raise ValueError('dpotri failed on input {}'.format(cholesky))
    upper_triangular_to_symmetric(inv)
    return inv


class GP:
    def __init__(self, x):
        self.x = x
        self.ndata = len(x[:, 0])
        self.ndim = len(x[0, :])
        self.covariane_matrix = np.zeros((self.ndata, self.ndata))
        self.inverse_covariance_matrix = np.zeros((self.ndata, self.ndata))
        self.pos_def = False
        self.ln_det_cov = 0
        pass

    def update(self, amplitude=None, length_scale=None, alpha=None, white_noise=None):
        amplitude2 = amplitude ** 2
        length_scale2 = length_scale ** 2
        white_noise2 = white_noise ** 2
        for i in range(self.ndata):
            self.covariane_matrix[i, i] = amplitude2 + white_noise2
            for j in range(i + 1, self.ndata):
                self.covariane_matrix[i, j] = self.kernel(self.x[i], self.x[j],
                                                          amplitude2=amplitude2, length_scale2=length_scale2,
                                                          alpha=alpha)
                self.covariane_matrix[j, i] = self.covariane_matrix[i, j]
        if not np.all(np.linalg.eigvals(self.covariane_matrix) > 0):
            self.pos_def = False
        else:
            self.pos_def = True
        if self.pos_def:
            _, self.ln_det_cov = np.linalg.slogdet(self.covariane_matrix)
            try:
                self.inverse_covariance_matrix = fast_positive_definite_inverse(self.covariane_matrix)
            except:
                self.pos_def = False

    def lnlikelihood(self, y, no_const=True):
        if not self.pos_def:
            return TINY_LIKELIHOOD
        y = np.array(y)
        if no_const:
            res = -y.transpose().dot(self.covariane_matrix.dot(y)) - self.ln_det_cov
        else:
            res = (-0.5 * y.transpose().dot(self.covariane_matrix.dot(y)) -
                   0.5 * self.ln_det_cov -
                   0.5 * self.ndata * np.log(2 * np.pi))
        if np.isnan(res) or np.isinf(res):
            return TINY_LIKELIHOOD
        return res

    @staticmethod
    def kernel(x1, x2, amplitude2=None, length_scale2=None, alpha=None):
        ndim = len(x1)
        res = amplitude2
        for i in range(ndim):
            res = res * (1 + (x1[i] - x2[i]) ** 2 / (2 * alpha[i] * length_scale2[i])) ** -alpha[i]
        return res
